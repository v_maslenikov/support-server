// Подключаем вебсокет
var geoip = require('geoip-lite');
var ua_parser = require('ua-parser');
var crypto = require('crypto');

var event_emitter = require('events').EventEmitter;
var util = require('util');

var mixin = require('merge-descriptors');
var sender_types = require('./system/sender_types');
var clients_array = require('./system/sockets_storage.js')();


var GUEST_NAME = 'Guest';

/**
 *
 * @param http_server
 * @param logger
 */
function http_socket_app(http_server, logger) {

    this.logger = logger;

    this.io = require('socket.io')(http_server);

    event_emitter.call(this);

    var app = this;

    app.EVENT_USER_CONNECTED = 'http_user_connected';
    app.EVENT_USER_DISCONNECTED = 'http_user_disconnected';
    app.EVENT_USER_LIST = 'http_user_list';
    app.EVENT_NEW_MESSAGE = 'http_new_message';


    app.TYPE_USER_LIST = 'user_list';
    app.TYPE_NEW_MESSAGE = 'new_message';

    this.emitUserList = function () {

        app.emit(
            app.EVENT_USER_LIST,
            clients_array.get(
                function (socket) {
                    return {
                        'ip': socket.address,
                        'geoip': socket.geo,
                        'token': socket.token,
                        'browser': socket.browser
                    }
                }
            )
        );

    };

    this.sendMessage = function (destination_client, sender_type, real_name, message) {
        destination_client.send(app.TYPE_NEW_MESSAGE, {
            sender_type: sender_type,
            sender_name: real_name,
            message: message
        });
    };

    this.handleOperatorMessage = function (source_client, destination_token, destination_message, sender_type) {


        var destination_client = clients_array.getByToken(destination_token);

        if (destination_client) {

            app.sendMessage(
                destination_client,
                sender_type,
                source_client.user.real_name,
                destination_message
            );

        } else {

            app.logger.error('HTTP-client with token %s not found', destination_token);

        }

    };


    app.on(
        app.EVENT_NEW_MESSAGE,
        function (client, message) {
            app.sendMessage(
                client,
                sender_types.CLIENT,
                GUEST_NAME,
                message
            );
        }
    );

    /**
     * Хэндлим подключение нового клиента
     */
    this.io.on('connection', function (client) {

        wrap_socket(client, function () {

            app.emit(app.EVENT_USER_CONNECTED, client);

            clients_array.push(client);

            var token = client.token;

            app.logger.info(
                'Client ' + client.address + ' connected:' +
                JSON.stringify({
                    'geoip': client.getGeo(),
                    'browser': client.getBrowser()
                })
            );

            client.on('disconnect', function () {

                app.logger.info(
                    'Client ' + client.address + ' disconnected:' +
                    JSON.stringify({
                        'geoip': client.getGeo(),
                        'browser': client.getBrowser()
                    })
                );

                app.emit(app.EVENT_USER_DISCONNECTED, client);

                clients_array.remove(client);

            });

            client.on('chat_message', function (message) {

                app.logger.info('Client `%s` sent message: `%s`', client.address, message);


                if (message == '1') {

                    for (var i = 0; i < 100; i++) {

                        app.emit(app.EVENT_NEW_MESSAGE, client, message);
                    }
                } else {
                    app.emit(app.EVENT_NEW_MESSAGE, client, message);
                }

            });

        });


    });

    /**
     * Оборачивает socket, заполняя его дополнительной информацией о клиенте
     * По завершению вызывает callback
     * @param socket
     * @param callback
     */
    function wrap_socket(socket, callback) {

        // Дополняем информацией
        socket.address = socket.handshake.address;

        socket.geo = geoip.lookup(socket.address);

        socket.browser = ua_parser.parseUA(socket.request.headers['user-agent'])
            .toString();
        /**
         * Генерируем token, для этого socket
         */
        crypto.randomBytes(16, function (ex, buf) {

            socket.token = buf.toString('hex');

            callback(socket);

        });

        socket.send = function (type, data) {
            app.logger.debug('Send to http-client %s data %j', socket.getAddress(), data);
            socket.emit(type, JSON.stringify({'data': data}));
        };

        socket.getAddress = function () {
            return socket.address;
        };

        socket.getGeo = function () {
            return socket.geo;
        };

        socket.getBrowser = function () {
            return socket.browser;
        };

        socket.getToken = function () {
            return socket.token;
        };

        return socket;

    }


    return this;

}


util.inherits(http_socket_app, event_emitter);

module.exports = http_socket_app;