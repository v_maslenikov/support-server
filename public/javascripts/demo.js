var loading = function () {
    // add the overlay with loading image to the page
    $('#overlay').fadeIn(200);
};

function hide_loading() {
    $('#overlay').fadeOut(200);
}
function show_loading_error(text) {
    $('#overlay').html('<div id="loading_error" class="alert alert-danger" align="center">' + text + '<br><button type="button" class="btn btn-tumblr" onclick="location.reload()">Обновить</button></div>');
}

$(function () {

    load_link(location.pathname);


    window.onpopstate = function (e) {
        if (e.state) {


            load_link(e.state);

        }
    };


});

function replace_blocks(blocks) {

    var key;

    for (key in blocks) {

        if (blocks.hasOwnProperty(key)) {

            block_name = key;
            block_element = $('#' + block_name + '_html');

            if (block_element.length) {

                block_element.
                    html(blocks[key]);

            } else {

                show_loading_error('Ошибка сервера, блока не существует');
                return;

            }

        }

    }

    $.AdminLTE.layout.activate();

}

var is_link_ignored = function (link) {

    var IGNORE_LINKS = ['#', '/exit'];

    var res = false;

    for (key in IGNORE_LINKS) {
        var ignore_link = IGNORE_LINKS[key];
        if (link.substring(0, ignore_link.length) == ignore_link) {
            res = true;
            break;
        }
    }

    return res;
};
var load_link = function load_link(pathname) {


    loading();


    setTimeout(function () {

        var path_with_trailing_slash = pathname + ((pathname.substring(pathname.length - 1) !== '/') ? '/' : '');

        var get_info_path = path_with_trailing_slash + 'get_info';

        $.get(get_info_path, function (blocks) {

            var block_name;
            var block_element;

            if (path_with_trailing_slash != window.history.state) {
                window.history.pushState(path_with_trailing_slash, "", path_with_trailing_slash);
            }

            replace_blocks(blocks);

            set_event_handlers();


            hide_loading();

        }, 'json').error(function () {
            show_loading_error('Не удалось загрузить страницу');
        });
    }, 1);
};

function loadStyleSheet(path, fn, scope) {
    var head = document.getElementsByTagName('head')[0], // reference to document.head for appending/ removing link nodes
        link = document.createElement('link');           // create the link node
    link.setAttribute('href', path);
    link.setAttribute('rel', 'stylesheet');
    link.setAttribute('type', 'text/css');

    var sheet, cssRules;
// get the correct properties to check for depending on the browser
    if ('sheet' in link) {
        sheet = 'sheet';
        cssRules = 'cssRules';
    }
    else {
        sheet = 'styleSheet';
        cssRules = 'rules';
    }

    var interval_id = setInterval(function () {                    // start checking whether the style sheet has successfully loaded
            try {
                if (link[sheet] && link[sheet][cssRules].length) { // SUCCESS! our style sheet has loaded
                    clearInterval(interval_id);                     // clear the counters
                    clearTimeout(timeout_id);
                    fn.call(scope || window, true, link);           // fire the callback with success == true
                }
            } catch (e) {
            } finally {
            }
        }, 10),                                                   // how often to check if the stylesheet is loaded
        timeout_id = setTimeout(function () {       // start counting down till fail
            clearInterval(interval_id);            // clear the counters
            clearTimeout(timeout_id);
            head.removeChild(link);                // since the style sheet didn't load, remove the link node from the DOM
            fn.call(scope || window, false, link); // fire the callback with success == false
        }, 15000);                                 // how long to wait before failing

    head.appendChild(link);  // insert the link node into the DOM and start loading the style sheet

    return link; // return the link node;
}

function set_event_handlers() {
    $('form[method=POST]').submit(function () {
        var post_link = $(this).attr('action');
        var post_data = $(this).serialize();

        var submit_buttons = $(this).find('button[type=submit]');
        var error_container = $('#error_container');
        var success_container = $('#success_container');

        submit_buttons.attr('disabled', 'disabled');
        error_container.hide(200);

        $.post(post_link, post_data, function (data) {

            submit_buttons.removeAttr('disabled');

            if (data.status) {

                error_container.hide(200);
                success_container.html(data.message).show(200);

            } else {

                success_container.hide(200);
                error_container.html(data.error_text).show(200);

            }

        }, 'json').error(function () {
            alert('Ошибка подключения к серверу!');
        });
        return false;
    });

    $('a[href]').each(function () {
        $(this).click(function () {
            var link_element = $(this);
            var link_href;
            if (link_href = link_element.attr('href')) {

                console.log(link_href);

                if (this.host != location.host || is_link_ignored(link_href)) {

                    return true;

                } else {

                    load_link(this.pathname);
                    return false;

                }
            } else {
                return true;
            }
        });
    });
}