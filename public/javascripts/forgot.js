$(function () {

    function show_alert(message) {
        var alert = $('#login_alert');
        alert.html(message).fadeIn(400);
    }

    $('#form_login').submit(function () {
        $('#forgot_btn').attr('disabled', 'disabled');

        var form_login = $(this);
        setTimeout(function () {
            var form_data = form_login.serialize();

            $.post(
                location.href,
                form_data,
                function (data) {
                    // Если авторизировано успешно - перебросим на главную
                    if (data.status) {
                        location.pathname = '/';
                    } else {
                        show_alert(data.error_text);
                        $('#forgot_btn').removeAttr('disabled');
                    }
                },
                'json'
            ).error(function () {
                    alert('Ошибка подключения к серверу!');
                    $('#forgot_btn').removeAttr('disabled');
                });


        }, 500);

        return false;
    });

})
