// port:3010

var SENDER_USER = 0;
var SENDER_OPERATOR = 1;

var socket = io();
socket.on('new_message', function (data) {
    var image;
    var name;
    data = JSON.parse(data);
    var d = new Date();
    if (data.data.sender_type == SENDER_OPERATOR) {
        name = data.data.sender_name;
        image = '/avatar/operator.png';
    } else {
        name = 'Guest';
        image = '/avatar/guest.png';
    }
    $('#messages').append(' <li class="media"><div class="media-body">' +
    '<div class="media"><a class="pull-left" href="#"><img class="media-object img-circle " src="' + image + '" />' +
    '</a><div class="media-body" >' + data.data.message + '<br /><small class="text-muted">' + name + ' | '
    + moment(d).format('HH:mm:ss')
    + ' </small>'
    + '<hr /></div></div></div></li>');

    $("#panel_height").stop().animate({scrollTop: $('#panel_height')[0].scrollHeight}, 1000);
});

$(function () {
    $('#panel_height').height($(document).height() - 300);
    $('#message_form').submit(function () {

        var text = $('#text_message').val();
        //

        socket.emit('chat_message', text);
        $('#text_message').val('');

        //alert('123');
        return false;
    });
});
