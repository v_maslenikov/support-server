/**
 * Main server file
 */

try {

    var server_info = {
        'homepage': 'http://google.com',
        'company_img': 'http://img.spiritix.eu/i/WDPo.png',
        'welcome_page': 'http://spiritix.eu'
    };
    /**
     * ИНИЦИАЛИЗИРУЕМ ВСЕ
     */
    var port = 8012;

    var console = require('./logger.js');

    var geoip = require('geoip-lite');


    var os = require('os');

    var md5 = require('MD5');

    var par = require('ua-parser');

    var crypto = require('crypto');

    var mysql = require('mysql');
    /**
     * MYSQL CONNECTION
     */
    var db_config = {
        host: 'localhost',
        user: 'root',
        password: '',
        database: 'support'
    };

    var connection;

    function handleDisconnect() {
        connection = mysql.createConnection(db_config); // Recreate the
        // connection, since
        // the old one cannot be reused.

        connection.connect(function (err) { // The http_server is either down
            if (err) {
                console.error('[MYSQL] Error connecting: ' + err.stack);
                setTimeout(handleDisconnect, 2000); // We introduce a delay
                return;
            }
            connection.query("set names utf8");
            console.log('[MYSQL] Connected as id ' + connection.threadId);
        }); // process asynchronous requests in the meantime.

        // If you're also serving http, display a 503 error.
        connection.on('error', function (err) {
            console.log('[MYSQL] Error: ' + err);
            if (err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the
                // MySQL http_server is
                // usually
                handleDisconnect(); // lost due to either http_server restart, or a
            } else { // connnection idle timeout (the wait_timeout
                throw err; // http_server variable configures this)
            }
        });
    };

    handleDisconnect();

    // Создаем веб-сервер
    var app = require('express')();
    var http = require('http').Server(app);
    // Подключаем вебсокет
    var io = require('socket.io')(http);

    // Sock clients_array stores info about connected client programms
    var sockStack = [];
    // Http clients_array stores info about connected http users
    var httpStack = [];
    // Удаление из стека подключния к ОПЕРАТОРНОМУ клиенту
    function removeFromSockStack(conn) {
        var index = sockStack.indexOf(conn);
        if (index > -1) {
            sockStack.splice(index, 1);
        }
    }

    // Удаление из стека подключния к HTTP пользователю
    function removeFromHttpStack(conn) {
        var index = httpStack.indexOf(conn);
        if (index > -1) {
            httpStack.splice(index, 1);
        }
    }

    // INITIALIZE
    console.log('[SERVER] AVAILABLE INTERFACES:');
    console.log(os.networkInterfaces());
    /**
     * ОБРАБОТКА КЛИЕНТОВ
     */
    function Client() {
        var token;
    }

    Client.list = function () {
        var list = [];
        for (i = 0; i < httpStack.length; i++) {
            var cl = httpStack[i];
            list.push({
                'ip': cl.address,
                'geoip': cl.geo,
                'token': cl.token,
                'browser': cl.browser
            });
        }
        console.log(list);
        return list;

    }

    /**
     * ОБРАБОТКА ПОЛЬЗОВАТЕЛЕЙ
     */
    function User(type) {
        var login;
        var pass;
        var token;
        this.getInfo = getAppleInfo;
    }

    User.login = function (login, pass, callback) {
        var query = 'SELECT `users`.*, UNIX_TIMESTAMP(`users`.`last_auth`) as `last_auth_int` FROM `users` WHERE `login` =  ? AND `pass` =  ? LIMIT 1';
        connection
            .query(
            query,
            [login, md5(md5(pass))],
            function (err, rows, fields) {
                if (err)
                    throw err;

                // console.log('The solution is: ', );
                if (rows.length > 0) {
                    connection
                        .query(
                        "UPDATE `users` SET `last_auth` = NOW() WHERE `id` = ?",
                        rows[0].id);
                    delete rows[0].pass;
                    // Gen new session id
                    crypto
                        .randomBytes(
                        32,
                        function (ex, buf) {
                            var token = buf
                                .toString('hex');
                            console
                                .log("Login and pass correct, he is logged. Session id: "
                                + token)

                            callback({
                                valid: true,
                                data: rows[0],
                                token: token
                            });
                        });

                } else {
                    callback({
                        valid: false
                    });
                }

            });
    }

    /**
     * ВЕБСЕРВЕР
     */
    app.get('/', function (req, res) {
        res.sendFile(__dirname + '/pages/index.html');
    });

    app.get('/jquery.min.js', function (req, res) {
        res.sendFile(__dirname + '/pages/jquery.min.js');
    });

    io.on('connection', function (socket) {
        socket.address = socket.handshake.address;

        console.log('[HTTP] Client ' + socket.address + ' connected');
        crypto.randomBytes(16, function (ex, buf) {
            var token = buf.toString('hex');
            socket.token = token;
            httpStack.push(socket);

            socket.geo = geoip.lookup(socket.address);
            socket.browser = par.parseUA(socket.request.headers['user-agent'])
                .toString();
            emitUserConnected(socket.address, socket.token, socket.geo, socket.browser);
            console.log('[HTTP][' + socket.address + '] GEOIP: '
            + JSON.stringify(socket.geo));
            console.log('[HTTP][' + socket.address + '] BROWSER: '
            + JSON.stringify(socket.browser));
            socket.on('disconnect', function () {
                emitUserDisconnected(token);
                removeFromHttpStack(socket);
                console.log('[HTTP]  Client ' + socket.address + ' disconnected');
                console.log('[HTTP] STACK');
                console.log(httpStack.length)
            });
            socket.on('chat_message', function (msg) {
                console.log('[HTTP][' + socket.address + '] Message: ' + msg);
                // 1 - USER SENDER
                if (msg == '1') {
                    for (i = 0; i < 100; i++) {

                        emitNewMessage(socket.token, 1, socket.address, msg);
                    }
                } else {
                    emitNewMessage(socket.token, 1, socket.address, msg);
                }

            });

        });

    });

    http.listen(3000, function () {
        console.log('[HTTP] Server binded at port 3000');
    });
    /**
     * ВЕБСЕРВЕР КОНЕЦ
     */

    // Работа с ОПЕРАТОРНЫМ клиентом
    var i = 0;

    var net = require('net');

    function emitUserList(list) {
        emitMessage({
            'type': 'user_list',
            'data': list
        });
    }

    function emitNewMessage(token, sender_type, sender_name, msg) {
        var arr = {
            'type': 'new_message',
            'data': {
                'token': token,
                'message': msg,
                'sender_type': sender_type,
                'sender_name': sender_name,
            }
        };

        /*if(from != undefined){
         arr.data.send = from;
         }*/
        emitHttpMessage(arr, token);
        emitMessage(arr);

    }

    function emitUserConnected(ip, token, geoip, browser) {
        emitMessage({
            'type': 'user_connected',
            'data': {
                'ip': ip,
                'geoip': geoip,
                'token': token,
                'browser': browser
            }
        });
    }

    function emitUserDisconnected(token) {
        emitMessage({
            'type': 'user_disconnected',
            'data': {
                'token': token
            }
        });
    }

    function emitMessage(jsonMessage) {
        var i = 0;
        for (inx in sockStack) {

            // Клиент получает сообщение только если авторизирован
            if (sockStack[inx].token != undefined) {
                i++;
                sockStack[inx].write(JSON.stringify(jsonMessage));
            }
        }
        console.log("[SOCK] Emitted new message: "
        + JSON.stringify(jsonMessage) + "\n\t\t   -> " + i
        + " clients recieved it");

    }

    function emitHttpMessage(jsonMessage, token) {
        var i = 0;
        for (inx in httpStack) {

            // Клиент получает сообщение только если авторизирован
            if (httpStack[inx].token == token) {
                httpStack[inx].emit('new_message', JSON.stringify(jsonMessage));
                console.log("[SOCK] Emitted new HTTP message: "
                + JSON.stringify(jsonMessage) + "\n\t\t   -> " + i
                + " clients recieved it");
                return;
            }

        }


    }

    var server = net.createServer(
        function (c) {
            console.log('[SOCK] Client ' + c.remoteAddress + ':'
            + c.remotePort + ' connected');
            //c.setNoDelay(true);
            sockStack.push(c);

            c.write(JSON.stringify(server_info));

            c.on('end', function () {
                // Delete this client from stack
                removeFromSockStack(c);

                console.log('Client disconnected');

            })

                .on(
                'data',
                function (data) {
                    console.log("Recieved data:" + data);

                    var res = JSON.parse(data);
                    switch (res['do']) {
                        case 'login':
                        {

                            User.login(res.login, res.pass, function (res) {
                                console.log("Login response: "
                                + JSON.stringify(res));
                                c.write(JSON.stringify(res));
                                if (res.valid) {
                                    c.token = res.token;
                                    c.realname = res.data.realname;
                                }

                            });
                            /*
                             * if (res.login != 'liquid') { } else {
                             * console.log("Login incorrect. Sending
                             * answer."); c.write(JSON.stringify({ "valid" :
                             * false, "reason" : "" })); }
                             */

                        }
                            break;
                        case 'get_users':
                        {
                            emitUserList(Client.list());
                        }
                            break;
                        case 'send_message':
                        {
                            emitNewMessage(res['token'], 0, c.realname, res['message']);

                        }
                            break;

                    }

                    // c.write();
                })

                .on('error', function (text) {
                    removeFromSockStack(c);
                    console.log(text);
                });

        }).listen(port, function () {

            console.log("[SOCK] Server binded at port " + port);

        }).on('error', function () {
            console.error("Bind error");
        });

    // http_server
    server.on('data', function (data) {

        console.log(data);

    });
} catch (err) {
    // (err);
    console.error(err);
}
