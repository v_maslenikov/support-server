var net = require('net');
var orm = require('./system/orm.js');

var crypto = require('crypto');

var event_emitter = require('events').EventEmitter;
var util = require('util');


var mixin = require('merge-descriptors');
var sender_types = require('./system/sender_types.js');

var server_info = {
    'homepage': 'http://google.com',
    'company_img': 'http://img.spiritix.eu/i/WDPo.png',
    'welcome_page': 'http://spiritix.eu'
};


/**
 * Хранилище активных sockets
 */
var sockets_array = require('./system/sockets_storage')();

function socket_app(logger) {

    var app = this;

    app.EVENT_NEW_MESSAGE = 'socket_new_message';
    app.EVENT_NEED_USER_LIST = 'socket_need_user_list';
    app.EVENT_USER_LOGGEDIN = 'socket_user_loggedin';
    app.EVENT_USER_UNLOGGEDIN = 'socket_user_unloggedin';

    app.TYPE_NEW_MESSAGE = 'new_message';
    app.TYPE_NEW_USER_CONNECTED = 'user_connected';
    app.TYPE_USER_DISCONNECTED = 'user_disconnected';
    app.TYPE_USER_LIST = 'user_list';

    this.logger = logger;

    this.server = net.createServer(onClientConnected).on('error', onServerError);

    this.start = function (port) {
        app.port = port;
        app.server.listen(port, onListenSuccess);
    };

    this.sendUserList = function (user_list) {
        app.sendAllClients(app.TYPE_USER_LIST, user_list);
    };

    this.sendAllClients = function (type, data) {


        var send_data = {
            type: type,
            data: data
        };

        var sockets = sockets_array.get();
        var socket;

        for (key in sockets) {
            socket = sockets[key];
            socket.send(send_data);
        }
    };

    this.handleClientMessage = function (http_client, message) {
        app.sendAllClients(
            app.TYPE_NEW_MESSAGE,
            {
                message: message,
                sender_type: sender_types.CLIENT,
                sender_name: '', // todo
                token: http_client.token
            }
        );
    };

    this.handleNewUserConnected = function (ip, token, geoip, browser) {
        app.sendAllClients(
            app.TYPE_NEW_USER_CONNECTED,
            {
                ip: ip,
                token: token,
                geoip: geoip,
                browser: browser
            }
        );
    };

    this.handleUserDisconnected = function (token) {
        app.sendAllClients(
            app.TYPE_USER_DISCONNECTED,
            {
                token: token
            }
        );
    };

    /**
     *
     */
    app.on(app.EVENT_NEW_MESSAGE, function (source_client, destination_token, message) {

        app.sendAllClients(app.TYPE_NEW_MESSAGE, {
            'message': message,
            'sender_type': sender_types.OPERATOR,
            'sender_name': source_client.user.real_name,
            'token': destination_token
        });
    });

    /**
     * Добавляет дополнительный функционал к клиентскому объекту
     * @param client
     */
    function wrapClientObject(client) {

        /**
         * Отправляет сериализированные данные клиенту
         * @param data
         */
        client.send = function (data) {
            app.logger.debug('Send to socket-client %s data %j', client.remoteAddress, data);
            client.write(JSON.stringify(data));
        };

        return client;
    }


    function onServerError(data) {

        app.logger.critical('Server error: %j', data);

    }

    function onListenSuccess() {

        app.logger.info('Server successfully binded at port %d', app.port);

    }

    /**
     * Обработчик подклчюения нового клиента к серверу
     * @param client
     */
    function onClientConnected(client) {

        app.logger.info('Client %s:%d connected', client.remoteAddress, client.remotePort);

        wrapClientObject(client);

        sockets_array.push(client);

        client.send(server_info);


        // Так как при отключении, нам неизвестна информация о клиенте - сохраним её сюда
        var remote_addr = client.remoteAddress;
        var remote_port = client.remotePort;

        client.on('end', onClientDisconnected).on('data', onClientData).on('error', onClientDisconnected);

        function onClientDisconnected() {

            /**
             * Если пользователь был авторизирован - оповестим о его отключении
             */
            if (client.user) {
                app.emit(app.EVENT_USER_UNLOGGEDIN, client.user);
            }

            // Delete this client from stack
            sockets_array.remove(client);

            app.logger.info('Client %s:%d disconnected', remote_addr, remote_port);


        }

        function onClientData(data) {

            app.logger.info('Recieved data from client %s: `%s`', remote_addr, data);

            var request = JSON.parse(data);


            switch (request['do']) {

                case 'login':

                    login(request.login, request.pass, function (result) {

                        if (result.valid) {

                            client.user = result.data;

                            client.token = result.token;

                            app.emit(app.EVENT_USER_LOGGEDIN, client.user);

                        }

                        client.send(result);

                    });

                    break;

                case 'get_users':

                    app.emit(app.EVENT_NEED_USER_LIST);
                    break;

                case 'send_message':

                    app.emit(app.EVENT_NEW_MESSAGE, client, request.token, request.message);
                    break;

            }


        }


    }

    util.inherits(app, event_emitter);

    return this;

}


function login(login, pass, callback) {

    orm.entities.user.getUserByLoginPass(login, pass, function (user) {

        if (user) {

            crypto.randomBytes(
                32,
                function (ex, buf) {
                    var token = buf
                        .toString('hex');


                    callback({
                        valid: true,
                        data: user,
                        token: token
                    });
                });


        } else {

            callback({valid: false});

        }

    });

}

util.inherits(socket_app, event_emitter);

module.exports = socket_app;