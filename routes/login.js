var express = require('express');
var router = express.Router();
var md5 = require('MD5');
var validator = require('validator');
var orm = require('../system/orm');
var crypto = require('crypto');
var merge = require('merge');
var mail_sender = require('../system/mail');

var STATUS_ERROR = 0;
var STATUS_OK = 1;


function prepare_error(error_message) {

    return JSON.stringify(
        {
            status: STATUS_ERROR,
            error_text: error_message
        }
    );

}

function prepare_success(message) {
    return JSON.stringify(
        {
            status: STATUS_OK,
            message: message
        }
    );

}

/**
 *
 * @param IncomingMessage req
 * @return bool
 */
function can_be_logged_in(req) {
    /**
     * Если пользователь авторизирован - он не может быть авторизирован повторноы
     */

    return !req.session.loggedin;
}

router.get('/', function (req, res, next) {

    if (!can_be_logged_in(req)) {

        res.redirect('/');

        return;

    }

    res.render('login/index');

});

router.get('/forgot', function (req, res, next) {

    res.render('login/forgot')
});

router.post('/forgot', function (req, res, next) {

    if (!req.body.email || !validator.isEmail(req.body.email)) {

        res.send(
            prepare_error('Вы не указали email, или указали его не верно')
        );

        return;

    }

    var email = req.body.email;

    orm.entities.user.find({
        where: {
            email: email
        }
    }).then(function (user) {
        if (user) {
            crypto.randomBytes(
                8,
                function (ex, buf) {
                    var user_data = {
                        email: user.email,
                        real_name: user.real_name,
                        login: user.login
                    };

                    var password = buf
                        .toString('hex');
                    user.updateAttributes({
                        'password': md5(password)
                    }).then(function () {
                        req.app.render('admin/mails/new_pass.dot', merge(user_data, {new_pass: password}), function (err, html) {

                            mail_sender.sendMail({
                                from: 'test@test.ru',
                                to: email,
                                subject: '[SupportSystem] Создан новый аккаунт',
                                text: html
                            });

                        });
                    });
                });
        }
    })


    return res.send(
        prepare_success('На почту оптравлено письмо с новым паролем')
    )

});

router.post('/', function (req, res, next) {

    if (!can_be_logged_in(req)) {

        res.send(JSON.stringify(
            {
                'status': false,
                'error_msg': 'Вы уже авторизированы, обновите страницу'
            }
        ));

        return;

    }

    var app = req.app;

    var user_entity = app.locals.orm.entities.user;

    if (req.body.login.length == 0 || req.body.password.length == 0) {

        res.send(JSON.stringify(
            {
                'status': false,
                'error_msg': 'Все поля обязательны к заполнению'
            }
        ));

        return;
    }

    user_entity.findOne({
        where: {
            'login': req.body.login,
            'password': md5(req.body.password),
            'role': 'admin',
            'deleted': false
        }
    }).then(function (user) {

        if (user) {

            req.session.user = user;
            req.session.loggedin = true;
            req.session.save();

            res.send(
                JSON.stringify(
                    {
                        'status': true
                    }
                )
            );

        } else {

            res.send(JSON.stringify(
                {
                    'status': false,
                    'error_msg': 'Пользователь не найден. Проверьте правильность ввода логина и пароля'
                }
            ))

        }


    }).error(function () {

        res.send(JSON.stringify(
            {
                'status': false,
                'error_msg': 'Ошибка базы данных'
            }
        ))

    });


});

module.exports = router;
