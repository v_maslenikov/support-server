var express = require('express');
var router = express.Router();

/**
 * Роутер выбирает на какую из страниц перенаправить пользователя (авторизация или административная часть)
 */
router.all('/exit', function (req, res, next) {
    if (req.session.loggedin) {
        req.session.loggedin = false;
        req.session.user = null;
        req.session.save();
    }

    console.log('exit?????????????????????????????????????');
    res.redirect('/');
});

router.get('/', function (req, res, next) {

    if (req.session.loggedin) {
        res.redirect('/admin')
    } else {
        res.redirect('/login');
    }

});

// Хак, для AJAX запросов админки
router.get('/get_info', function (req, res, next) {

    if (req.xhr && req.session.loggedin) {
        res.redirect('/admin');
    }

});


module.exports = router;
