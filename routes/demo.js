var express = require('express');
var router = express.Router();

/**
 * Роутер выбирает на какую из страниц перенаправить пользователя (авторизация или административная часть)
 */
router.get('/', function (req, res, next) {

    res.render('demo');

});

module.exports = router;
