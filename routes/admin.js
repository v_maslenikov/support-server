var express = require('express');
var router = express.Router();
var orm = require('../system/orm');
var moment = require('moment');
var util = require('util');
var merge = require('merge');
var mail_sender = require('../system/mail');
var crypto = require('crypto');
var md5 = require('MD5');


var STATISTICS_DAYS_COUNT = 15;
var DAY_FORMAT = 'YYYY-MM-DD';
var DAY_TIME_FORMAT = 'YYYY-MM-DD HH:mm';

var STATUS_ERROR = 0;
var STATUS_OK = 1;

var allowed_roles = ['operator', 'admin'];

var app;

var rendered_blocks = {};

var logger = require('intel');

router.setLogger = function setLogger(new_logger) {
    logger = new_logger;
};

function prepare_error(error_message) {

    return JSON.stringify(
        {
            status: STATUS_ERROR,
            error_text: error_message
        }
    );

}

function prepare_success(message) {
    return JSON.stringify(
        {
            status: STATUS_OK,
            message: message
        }
    );

}

function getDateIntervalObject(start, end) {

    var interval = {};

    var current_day = start;


    // Заполним нулями, дни которых нет в базе
    while (current_day <= end) {

        var moment_date;

        /** @var Moment */
        moment_date = moment(current_day);
        interval[moment_date.format(DAY_FORMAT)] = {};

        current_day.setDate(current_day.getDate() + 1);
    }

    return interval;

}

function formatAllDates(record) {

    var new_record = record;

    for (var property in record.dataValues) {

        if (
            typeof(record[property]) === 'object'
            && record[property] instanceof Date
        ) {
            record.dataValues[property] = moment(
                record[property]
            ).format(
                DAY_TIME_FORMAT
            );
        }

    }

    return new_record;
}

function fillIntervalByValues(interval_object, values, field_name) {

    var result = interval_object;
    for (day in interval_object) {

        result[day].day = day;

        if (values[day] && values[day][field_name]) {

            result[day][field_name] = values[day][field_name];

        } else {

            result[day][field_name] = 0;

        }
    }

    return result;

}

function format_title(title_name) {
    return title_name + ' | ' + app.locals.sitename;
}

function render_blocks(blocks, req, callback) {

    var current_index = 0;

    // Передаем инфу шаблонам
    req.app.locals.user = req.session.user;

    var process_rendered = function (err, html) {

        rendered_blocks[blocks[current_index]] = html;

        if (typeof blocks[current_index + 1] !== 'undefined') {

            req.app.render('admin/' + blocks[++current_index], {req: req}, process_rendered);

        } else {

            callback(rendered_blocks);

        }
    };

    req.app.render('admin/' + blocks[current_index], process_rendered);

}

/* GET users listing. */
router.all('/*', function (req, res, next) {

    app = req.app;

    if (!req.session.loggedin) {

        res.redirect('/');

    } else {

        next();

    }

});

router.all('/*', function (req, res, next) {

    if (!req.xhr) {

        res.render('admin/index');

    } else {

        next();

    }

});

/**
 * Подготовка контента для разных страниц
 */
router.get('/get_info', function (req, res, next) {


    orm.entities.client_session.count({
        where: {is_active: true}
    }).then(function (current_online_count) {

        orm.entities.user.count({
            where: {is_online: true}
        }).then(function (online_operators_count) {

            var start = new Date();
            start.setUTCHours(0, 0, 0, 0);

            var end = new Date();
            end.setUTCHours(23, 59, 59, 999);

            orm.entities.client_session.count({
                where: orm.sequelize.and(
                    ['created_at >= ?', start],
                    ['created_at <= ?', end]
                )
            }).then(function (today_visits_count) {


                orm.entities.client_session_message.count({
                    where: orm.sequelize.and(
                        ['created_at >= ?', start],
                        ['created_at <= ?', end]
                    )
                }).then(function (today_messages_count) {

                    orm.sequelize.query(
                        'SELECT DATE_FORMAT(`created_at`, \'%Y-%m-%d\') as `day`, COUNT(*) as `sessions_count`' +
                        'FROM `client_session`' +
                        'WHERE `created_at` BETWEEN DATE_SUB(NOW(), INTERVAL ' + STATISTICS_DAYS_COUNT + ' DAY) AND NOW() ' +
                        'GROUP BY `day`',
                        {type: orm.sequelize.QueryTypes.SELECT}
                    ).then(function (session_statistics_per_days) {

                            orm.sequelize.query(
                                'SELECT DATE_FORMAT(`created_at`, \'%Y-%m-%d\') as `day`, COUNT(*) as `messages_count`' +
                                'FROM `client_session_messages`' +
                                'WHERE `created_at` BETWEEN DATE_SUB(NOW(), INTERVAL ' + STATISTICS_DAYS_COUNT + ' DAY) AND NOW() ' +
                                'GROUP BY `day`',
                                {type: orm.sequelize.QueryTypes.SELECT}
                            ).then(function (messages_statistics_per_days) {

                                    var end = new Date();

                                    var statistics_with_date_keys = {};

                                    var session_stat_by_day;

                                    for (var key in session_statistics_per_days) {

                                        session_stat_by_day = session_statistics_per_days[key];

                                        if (statistics_with_date_keys[session_stat_by_day.day] == undefined) {

                                            statistics_with_date_keys[session_stat_by_day.day] = {};

                                        }

                                        statistics_with_date_keys[session_stat_by_day.day].sessions_count = session_stat_by_day.sessions_count;
                                    }

                                    for (var key in messages_statistics_per_days) {

                                        session_stat_by_day = messages_statistics_per_days[key];

                                        if (statistics_with_date_keys[session_stat_by_day.day] == undefined) {

                                            statistics_with_date_keys[session_stat_by_day.day] = {};

                                        }


                                        statistics_with_date_keys[session_stat_by_day.day].messages_count = session_stat_by_day.messages_count;
                                    }

                                    var start = new Date();

                                    start.setDate(end.getDate() - STATISTICS_DAYS_COUNT);

                                    var interval_object = getDateIntervalObject(start, end);

                                    var statitics_with_all_keys = fillIntervalByValues(interval_object, statistics_with_date_keys, 'sessions_count');

                                    statitics_with_all_keys = fillIntervalByValues(interval_object, statistics_with_date_keys, 'messages_count');

                                    req.app.render('admin/pages/index', {
                                        statistics: {
                                            current_online_count: current_online_count,
                                            online_operators_count: online_operators_count,
                                            today_visits_count: today_visits_count,
                                            today_messages_count: today_messages_count,
                                            statistics_per_days: statitics_with_all_keys
                                        }
                                    }, function (err, html) {

                                        rendered_blocks['content'] = html;
                                        rendered_blocks['title'] = format_title('Главная страница')

                                        next();

                                    });
                                });
                        }
                    )
                    ;
                });

            });

        });


    });

});

router.get('/users/get_info', function (req, res, next) {
    orm.entities.user.findAll({
        where: {
            deleted: false
        }
    }).then(function (users) {

        var users_with_formatted_date = [];

        for (key in users) {
            if (users.hasOwnProperty(key)) {
                users_with_formatted_date[key] = formatAllDates(users[key]);
            }
        }


        req.app.render('admin/pages/users', {
            users: users_with_formatted_date
        }, function (err, html) {

            rendered_blocks['content'] = html;
            rendered_blocks['title'] = format_title('Пользователи');

            next();

        });

    });
});

router.get('/users/add/get_info', function (req, res, next) {
    req.app.render('admin/pages/users_add', function (err, html) {

        rendered_blocks['content'] = html;
        rendered_blocks['title'] = format_title('Пользователи');

        next();

    });

});

router.post('/users/add', function (req, res, next) {
    if (req.body.email.length == 0 || req.body.login == 0 || req.body.real_name.length == 0) {

        res.send(prepare_error('Все поля должны быть заполнены'));
        return;
    }

    var login = req.body.login;
    var email = req.body.email;
    var real_name = req.body.real_name;
    var role = req.body.role;

    if (allowed_roles.indexOf(role) == -1) {

        res.send(prepare_error('Выберите корректную роль '));
        return;
    }

    orm.entities.user.find({
        where: orm.sequelize.and(
            orm.sequelize.or(
                {login: login},
                {email: email}
            ),
            {deleted: false}
        )
    }).then(function (duplicate_data) {

        if (duplicate_data != null) {
            res.send(
                prepare_error('Пользователь с таким логином, или электронной почтой уже существует')
            );
            return;
        }


        crypto.randomBytes(
            8,
            function (ex, buf) {

                var password = buf
                    .toString('hex');

                var user_data = {
                    'login': login,
                    'email': email,
                    'real_name': real_name,
                    'role': role,
                    'password': md5(password)
                };

                orm.entities.user.create(
                    user_data
                ).then(function () {

                        req.app.render('admin/mails/new_user.dot', merge(user_data, {real_pass: password}), function (err, html) {

                            mail_sender.sendMail({
                                from: 'test@test.ru',
                                to: email,
                                subject: '[SupportSystem] Создан новый аккаунт',
                                text: html
                            }, function (err, info) {
                                if (err) {

                                    logger.error(err);

                                    res.send(
                                        prepare_error('Кажется у нас произошли проблемы с отправкой почты пользователю, мы уже знаем об ошибке')
                                    );

                                } else {

                                    logger.info(info);

                                    res.send(
                                        prepare_success('Отлично, пользователь зарегистрирован :)')
                                    );

                                }
                            });


                        });
                    });
            });

    });
});


router.post('/users/edit', function (req, res, next) {
    if (req.body.email.length == 0 || req.body.login == 0 || req.body.real_name.length == 0 || req.body.id.length == 0) {

        res.send(prepare_error('Все поля должны быть заполнены'));
        return;
    }

    var login = req.body.login;
    var email = req.body.email;
    var real_name = req.body.real_name;
    var role = req.body.role;
    var user_id = parseInt(req.body.id);

    if (allowed_roles.indexOf(role) == -1) {

        res.send(prepare_error('Выберите корректную роль '));
        return;
    }

    if (isNaN(user_id) || user_id < 0) {

        res.send(prepare_error('Укажите корректный идентификатор редактируемого пользователя'));
        return;

    }

    orm.entities.user.find({
        where: orm.sequelize.and(
            orm.sequelize.or(
                {login: login},
                {email: email}
            ), {
                id: {
                    not: user_id
                }
            })

    }).then(function (duplicate_data) {

        if (duplicate_data != null) {
            res.send(
                prepare_error('Пользователь с таким логином, или электронной почтой уже существует')
            );
            return;
        }

        orm.entities.user.find({
            where: {
                id: user_id
            }
        }).then(function (edit_user) {


            if (edit_user) { // if the record exists in the db
                edit_user.updateAttributes({
                    'login': login,
                    'email': email,
                    'real_name': real_name,
                    'role': role
                }).then(function () {

                    res.send(
                        prepare_success('Отлично, пользователь отредактирован')
                    );
                    return;

                });
            }
        });

    });

})
;

router.param('user_id', function (req, res, next, user_id) {

    orm.entities.user.findById(user_id).then(function (user) {
        if (!user || user.deleted) {

            var err = new Error();
            err.status = 404;
            next(err);

        } else {

            req.user = formatAllDates(user);
            next();
        }
    });


});

router.post('/users/delete/:user_id', function (req, res, next) {

    req.user.updateAttributes({
        deleted: true
    }).then(function () {
        res.send(
            prepare_success('Пользователь успешно удален')
        )
    });
});


router.get('/users/profile/:user_id/get_info', function (req, res, next) {

    var user = req.user;
    req.app.render('admin/pages/users_edit', {
        user: user
    }, function (err, html) {

        rendered_blocks['content'] = html;
        rendered_blocks['title'] = format_title('Пользователи');

        next();

    });

});

router.get('/sessions/get_info', function (req, res, next) {

    var start = new Date();
    start.setUTCHours(0, 0, 0, 0);
    req.where = ['created_at >= ?', start];
    req.template_vars = {};
    next();
});

router.param('session_id', function (req, res, next, session_id) {

    orm.entities.client_session.findById(session_id).then(function (session) {
        if (session) {

            req.client_session = formatAllDates(session);
            next();

        } else {

            var err = new Error();
            err.status = 404;
            next(err);

        }
    });

});


router.get('/sessions/messages/:session_id/get_info', function (req, res, next) {

    var session = req.client_session;

    orm.entities.client_session_message.findAll({
        where: {
            client_session_id: session.id
        }
    }).then(function (messages) {

        var messages_with_formatted_date = [];

        var unique_users_ids = [];

        var key;

        for (key in messages) {

            if (messages.hasOwnProperty(key)) {

                var message = messages[key];

                messages_with_formatted_date[key] = formatAllDates(message);

                if (message.user_id != null && unique_users_ids.indexOf(message.user_id) === -1) {

                    unique_users_ids.push(message.user_id);

                }
            }
        }

        orm.entities.user.findAll({
            where: {
                id: unique_users_ids
            }
        }).then(function (users) {

            var users_with_id_keys = {};

            for (key in users) {
                users_with_id_keys[users[key].id] = users[key];
            }

            req.app.render('admin/pages/session_messages', {
                session: session,
                messages: messages_with_formatted_date,
                users: users_with_id_keys
            }, function (err, html) {

                rendered_blocks['content'] = html;
                rendered_blocks['title'] = format_title('Пользователи');

                next();

            });
        });


    });


});

router.param('from', function (req, res, next, from) {

    if ((from = parseInt(from)) && !isNaN(from) && from > 0) {

        req.range_from = from;

        next()

    } else {
        next(new Error('From argument must be a number'));
    }
});

router.param('to', function (req, res, next, to) {

    if ((to = parseInt(to)) && !isNaN(to) && to > 0) {

        req.range_to = to;

        next()

    } else {
        next(new Error('To argument must be a number'));
    }


});


router.get('/sessions/range/:from/:to/get_info', function (req, res, next) {

    if (req.range_from > req.range_to) {
        next(new Error('Argument "from" must be greater than "to"'));
    }

    var start = new Date(req.range_from * 1000);

    var end = new Date(req.range_to * 1000);


    req.where = ['created_at >= ? AND created_at <= ?', start, end];

    req.template_vars = {range: {from: req.range_from, to: req.range_to}};

    next();

});


router.get('/sessions/*', function (req, res, next) {
    if (!req.where) {
        next();
    } else {

        orm.entities.client_session.findAll({
            where: req.where
        }).then(function (client_sessions) {

            var client_sessions_with_formatted_date = {};

            for (key in client_sessions) {
                if (client_sessions.hasOwnProperty(key)) {
                    client_sessions_with_formatted_date[key] = formatAllDates(client_sessions[key]);
                }
            }

            req.app.render('admin/pages/sessions', merge({client_sessions: client_sessions_with_formatted_date}, req.template_vars), function (err, html) {

                console.log(err);
                rendered_blocks['content'] = html;

                rendered_blocks['title'] = format_title('Архив сессий');

                next();

            })


        });
    }
});

/**
 * Получение основных данных
 */
router.get('/*', function (req, res, next) {

    var blocks = ['user_panel', 'sidebar'];

    render_blocks(blocks, req, function () {
        res.json(rendered_blocks);
    });

});

router.use(function (error, req, res, next) {
    var blocks = ['user_panel', 'sidebar'];

    if (error.status == 404) {


        render_blocks(blocks, req, function () {
            req.app.render('admin/404', function (err, html) {

                rendered_blocks['content'] = html;
                rendered_blocks['title'] = format_title('Страница не найдена');
                res.json(rendered_blocks);
            })

        });

    } else {

        render_blocks(blocks, req, function () {
            req.app.render('admin/500', function (err, html) {

                rendered_blocks['content'] = html;
                rendered_blocks['title'] = format_title('Ошибка сервера');

                res.json(rendered_blocks);
            })

        });
        console.error(error);
        logger.error(error.name + ': ' + error.message);

    }
});


module.exports = router;