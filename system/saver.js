var orm = require('./orm');

/**
 * Модуль сохранения чатов в
 * @param logger
 */
var saver = function (logger) {

        this.logger = logger;

        // Указываем что все старые чаты уже неактивны
        orm.entities.client_session.update({
            is_active: false
        }, {
            where: {}
        });

        // Указываем что все операторы не в сети
        orm.entities.user.update({
            is_online: false
        }, {
            where: {}
        });

        app = this;

        this.handleNewClient = function (info) {

            orm.entities.client_session.create(
                info
            );
        };

        this.handleNewMessage = function (message, client_token, user_id) {

            orm.entities.client_session.findOne({where: {token: client_token}}).then(function (client_session) {
                    if (client_session) {

                        orm.entities.client_session_message.create({
                            message: message,
                            client_session_id: client_session.id,
                            user_id: user_id
                        });

                    } else {

                        app.logger.error('Client session with token %s, doesn\'t exists', client_token);

                    }
                }
            );
        };

        this.handleClientDisconnect = function (client) {

            orm.entities.client_session.update({
                is_active: false
            }, {
                where: {token: client.token}
            });

        };

        this.handleUserLoggedin = function (user) {

            user.update({
                is_online: true
            });

        };


        this.handleUserUnloggedin = function (user) {
            user.update({
                is_online: false
            });
        }
    }
    ;


module.exports = saver;
