var sequelize = require('sequelize');
var md5 = require('MD5');

var logger = require('intel');

var orm = {};

orm.setLogger = function (intel_logger) {
    logger = intel_logger;
}

orm.sequelize = new sequelize('support_2', 'root', '', {
    host: 'localhost',
    dialect: 'mysql',

    logging: function (str) {

        logger.debug(str);

    },

    timezone: '+02:00',

    pool: {
        max: 5,
        min: 0,
        idle: 10000
    },
    define: {underscored: true}
});


orm.entities = {};

orm.entities.user = orm.sequelize.define('user', {
    login: {
        type: sequelize.STRING(64)
    },
    password: {
        type: sequelize.STRING(32)
    },
    real_name: {
        type: sequelize.STRING,
        field: 'realname',
        allowNull: false
    },
    email: {
        type: sequelize.STRING,
        field: 'email',
        allowNull: false
    },
    last_auth: {
        type: sequelize.DATE,
        allowNull: true
    },
    role: {
        type: sequelize.ENUM('admin', 'operator'),
        defaultValue: 'operator'
    },
    is_online: {
        type: sequelize.BOOLEAN(),
        allowNull: false,
        defaultValue: false
    },
    deleted: {
        type: sequelize.BOOLEAN(),
        allowNull: false,
        defaultValue: false
    }
}, {

    freezeTableName: true,
    classMethods: {
        getUserByLoginPass: function (login, password, callback) {
            return orm.entities.user.findOne({
                where: {
                    login: login,
                    password: md5(password)
                }
            }).then(function (user) {
                callback(user);
            });
        }
    }
});


orm.entities.client_session = orm.sequelize.define('client_session', {
    ip: {
        type: sequelize.STRING(16)
    },
    geoip: {
        type: sequelize.STRING(255)
    },
    token: {
        type: sequelize.STRING(32)
    },
    browser: {
        type: sequelize.STRING(255)
    },
    is_active: {
        type: sequelize.BOOLEAN(),
        allowNull: false,
        defaultValue: true
    }
}, {
    freezeTableName: true
});


orm.entities.client_session_message = orm.sequelize.define('client_session_message', {
    message: {
        type: sequelize.TEXT()
    }
}, {
    updatedAt: false
});

orm.entities.client_session_message.belongsTo(
    orm.entities.client_session
);

orm.entities.client_session.hasMany(
    orm.entities.client_session_message
);

orm.entities.client_session_message.belongsTo(
    orm.entities.user
);

orm.entities.user.hasMany(
    orm.entities.client_session_message
);

orm.sequelize.sync();


module.exports = orm;