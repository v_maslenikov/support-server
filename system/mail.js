var nodemailer = require('nodemailer');
var pickup_transport = require('nodemailer-pickup-transport')

module.exports = nodemailer.createTransport(
    pickup_transport(
        {
            directory: './logs/mails/'
        }
    )
);