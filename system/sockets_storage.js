var sockets_storage = function () {
    return {

        sockets_array: [],

        // Удаление из массива подключния к HTTP пользователю
        remove: function (socket) {

            var index = this.sockets_array.indexOf(socket);

            if (index > -1) {
                this.sockets_array.splice(index, 1);
            }
        },

        push: function (socket) {

            this.sockets_array.push(socket);

        },

        get: function (converter) {

            var list = [];
            for (i = 0; i < this.sockets_array.length; i++) {

                var socket = this.sockets_array[i];

                var list_value;

                if (converter) {

                    list_value = converter(socket)

                } else {

                    list_value = socket;

                }

                list.push(list_value);

            }

            return list;
        },

        getByToken: function (token) {

            var i;

            for (i = 0; i < this.sockets_array.length; i++) {

                if (this.sockets_array[i].token == token) {
                    return this.sockets_array[i];
                }
            }

        }

    };
};


module.exports = sockets_storage;
