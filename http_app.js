/**
 * HTTP Server.
 * @type {*|exports}
 */
var express = require('express');

var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var randomstring = require('randomstring');


var http_app = express();

http_app.setLogger = function setLogger(new_logger) {
    logger = new_logger;
}


/***
 * Configurations
 */
http_app.locals.sitename = 'SupportSystems';


// view engine setup
var engine = require('express-dot-engine');
http_app.engine('dot', engine.__express);
http_app.set('views', path.join(__dirname, './views'));
http_app.set('view engine', 'dot');


// uncomment after placing your favicon in /public

http_app.use(bodyParser.json());
http_app.use(bodyParser.urlencoded({extended: false}));
http_app.use(cookieParser());

/**
 * Setup session
 */
http_app.use(session({
    name: 'SESSION',
    proxy: true,
    resave: true,
    saveUninitialized: true,
    genid: function () {
        return randomstring.generate(32) // use UUIDs for session IDs
    },
    secret: 'hwr4ehrlkh4rklhl4kjhrlk3hrk34jhj'
}));

/**
 * Setup static files (css, js's)
 */
http_app.use(express.static(path.join(__dirname, 'public')));

/**
 * Routing
 */

var routes = require('./routes/index');
http_app.use('/', routes);

var login = require('./routes/login');
http_app.use('/login', login);

var admin = require('./routes/admin');
admin.setLogger(require('intel').getLogger('ADMIN'));

http_app.use('/admin', admin);

var demo = require('./routes/demo');
http_app.use('/demo', demo);


/**
 * Database connection
 */

http_app.locals.orm = require('./system/orm.js');


/**
 * 404
 */
http_app.use(function (req, res, next) {

    var err = new Error('Not Found');
    err.status = 404;
    next(err);

});

// error handlers

// development error handler
// will print stacktrace
if (http_app.get('env') === 'development') {
    http_app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        console.log(err);
        res.render('404', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
http_app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('404', {
        message: err.message,
        error: {}
    });
});


module.exports = http_app;
